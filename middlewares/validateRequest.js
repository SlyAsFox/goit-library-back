const { errorsEnum, CustomError } = require( '../errors' );

const validateRequest = ( schema ) => {
    return ( req, res, next ) => {
        const { error } = schema.validate( req.body );

        if ( error ) {
            throw new CustomError( errorsEnum.VALIDATION_ERROR, error.details[0].message );
        }
        next();
    };
};

module.exports = validateRequest;
