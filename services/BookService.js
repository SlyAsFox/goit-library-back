const { Book } = require( '../models/Book' );
const { CustomError, errorsEnum } = require( '../errors' );

/**
 * Create a new book.
 * @param {Object} data - The data for the book.
 * @param {string} data.title - The title of the book.
 * @param {string} data.author - The author of the book.
 * @returns {Promise<Book>} A Promise that resolves to the created book.
 * @throws {Error} If an error occurs during database operations.
 */
const create = async ( data ) => {
    const { title, author } = data;

    return Book.create( {
        title,
        author
    } );
};

/**
 * Updates a book by its ID.
 *
 * @param {string} id - The ID of the book to update.
 * @param {Object} data - The data containing the updated book fields.
 * @param {string} data.title - The updated title of the book.
 * @param {string} data.author - The updated author of the book.
 * @returns {Promise<Object>} The updated book object.
 * @throws {CustomError} If the book with the given ID is not found.
 * @throws {Error} If an error occurs during database operations.
 */
const updateById = async ( id, data ) => {
    const { title, author } = data;

    const updatedBook = await Book.findByIdAndUpdate(
        id,
        { title, author },
        { new: true, lean: true }
    );

    if ( !updatedBook ) {
        throw new CustomError( errorsEnum.BOOK_NOT_FOUND, id );
    }

    return updatedBook;
};

/**
 * Delete a book by ID.
 * @param {string} id - The ID of the book to delete.
 * @returns {Promise<Book>} - A promise that resolves to the deleted book.
 * @throws {CustomError} If the book with the provided ID is not found.
 * @throws {Error} If an error occurs during database operations.
 */
const deleteById = async ( id ) => {
    const deletedBook = await Book.findByIdAndDelete( id );

    if ( !deletedBook ) {
        throw new CustomError( errorsEnum.BOOK_NOT_FOUND, id );
    }

    return deletedBook;
};

/**
 * Find a book by its ID.
 *
 * @param {string} id - The ID of the book.
 * @returns {Promise<Book>} The book object.
 * @throws {CustomError} If the book is not found.
 * @throws {Error} If an error occurs during database operations.
 */
const findById = async ( id ) => {
    const book = await Book.findById( id );

    if ( !book ) {
        throw new CustomError( errorsEnum.BOOK_NOT_FOUND, id );
    }

    return book;
};

/**
 * Retrieves paginated data from the Book collection.
 *
 * @async
 * @param {Object} [opts={}] - Options for pagination.
 * @param {number} [opts.page=1] - The page number to retrieve.
 * @param {number} [opts.limit=10] - The maximum number of items per page.
 * @returns {Promise<Object>} The paginated data.
 * @throws {Error} If an error occurs during database operations.
 */
const getPage = async ( opts = {} ) => {
    const MAX_PAGE_LIMIT_VALUE = 1000;
    const page = parseInt( opts.page ) || 1;
    let limit = parseInt( opts.limit ) || 10;

    if ( limit > MAX_PAGE_LIMIT_VALUE ) {
        limit = MAX_PAGE_LIMIT_VALUE;
    }

    const books = await Book.find().skip( ( page - 1 ) * limit ).limit( page * limit ).lean();
    const total = await Book.countDocuments();

    return {
        data: books,
        totalPages: Math.ceil( total / limit ),
        currentPage: page
    };
};

module.exports = {
    create, getPage, updateById, deleteById, findById
};
