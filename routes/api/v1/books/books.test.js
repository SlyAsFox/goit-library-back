const { app } = require( '../../../../app' ); // Assuming this is the file where your Express app is initialized
const request = require( 'supertest' );
const PATH = '/api/v1';
const { errorsEnum, errors, CustomError } = require( '../../../../errors' );

// Mock the BookService methods
jest.mock( '../../../../services/BookService' );
const BookService = require( '../../../../services/BookService' );

describe( 'Books Routes', () => {
    beforeEach( () => {
        jest.clearAllMocks();
    } );

    describe( 'POST /books', () => {
        it( 'should create a new book', async () => {
            const bookData = {
                title: 'The Great Gatsby',
                author: 'F. Scott Fitzgerald'
            };

            const createdBook = {
                _id: '1234567890',
                title: bookData.title,
                author: bookData.author
            };

            // Mock the BookService.create method
            BookService.create.mockResolvedValue( createdBook );

            const response = await request( app )
                .post( `${PATH}/books` )
                .send( bookData );

            expect( response.status ).toBe( 201 );
            expect( response.body.data ).toEqual( createdBook );
            expect( BookService.create ).toHaveBeenCalledWith( bookData );
        } );

        it( 'should return 415 Validation error if invalid request body', async () => {
            const bookData = {
                title: 'The Great Gatsby',
                // Missing the author field
            };

            const expectedError = errors[errorsEnum.VALIDATION_ERROR]( '"author" is required' );

            const response = await request( app )
                .post( `${PATH}/books` )
                .send( bookData );

            expect( response.status ).toBe( expectedError.code );
            expect( response.body.error.id ).toBe( expectedError.id );
            expect( response.body.error.message ).toBe( expectedError.message );
            expect( BookService.create ).not.toHaveBeenCalled();
        } );
    } );

    describe( 'PUT /books/:bookId', () => {
        it( 'should update an existing book', async () => {
            const bookId = '123';
            const bookData = {
                title: 'New Title',
                author: 'New Author'
            };

            const updatedBook = {
                _id: bookId,
                title: bookData.title,
                author: bookData.author
            };

            // Mock the BookService.create method
            BookService.updateById.mockResolvedValue( updatedBook );

            const response = await request( app )
                .put( `${PATH}/books/${bookId}` )
                .send( bookData );

            expect( response.status ).toBe( 200 );
            expect( response.body.data ).toEqual( updatedBook );
            expect( BookService.updateById ).toHaveBeenCalledWith( bookId, bookData );
        } );

        it( 'should return 415 Validation error if invalid request body', async () => {
            const bookId = 'someId1342';
            const bookData = {
                title: 'The Great Gatsby',
                // Missing the author field
            };

            const expectedError = errors[errorsEnum.VALIDATION_ERROR]( '"author" is required' );

            const response = await request( app )
                .put( `${PATH}/books/${bookId}` )
                .send( bookData );

            expect( response.status ).toBe( expectedError.code );
            expect( response.body.error.id ).toBe( expectedError.id );
            expect( response.body.error.message ).toBe( expectedError.message );
            expect( BookService.create ).not.toHaveBeenCalled();
        } );

        it( 'should return 404 Book not found error if bookId does not exist', async () => {
            const bookId = 'someId134231';
            const bookData = {
                title: 'New Title',
                author: 'New Author',
            };

            const expectedError = errors[errorsEnum.BOOK_NOT_FOUND]( bookId );
            BookService.updateById.mockRejectedValue( new CustomError( errorsEnum.BOOK_NOT_FOUND, bookId ) );

            const response = await request( app )
                .put( `${PATH}/books/${bookId}` )
                .send( bookData );

            expect( response.status ).toBe( expectedError.code );
            expect( response.body.error.id ).toBe( expectedError.id );
            expect( response.body.error.message ).toBe( expectedError.message );
            expect( BookService.updateById ).toHaveBeenCalledWith( bookId, bookData );
        } );
    } );

    describe( 'GET /books/:bookId', () => {
        it( 'should return the book with the given bookId', async () => {
            const bookData = {
                _id: '123456789',
                title: 'Mock Title',
                author: 'Mock Author'
            };
            BookService.findById.mockResolvedValue( bookData );

            const response = await request( app )
                .get( `${PATH}/books/${bookData._id}` );

            expect( response.status ).toBe( 200 );
            expect( response.body.data ).toEqual( bookData );
            expect( BookService.findById ).toHaveBeenCalledWith( bookData._id );
        } );

        it( 'should return 404 Book not found error if bookId does not exist', async () => {
            const bookId = 'someId134231';

            const expectedError = errors[errorsEnum.BOOK_NOT_FOUND]( bookId );
            BookService.findById.mockRejectedValue( new CustomError( errorsEnum.BOOK_NOT_FOUND, bookId ) );

            const response = await request( app )
                .get( `${PATH}/books/${bookId}` );

            expect( response.status ).toBe( expectedError.code );
            expect( response.body.error.id ).toBe( expectedError.id );
            expect( response.body.error.message ).toBe( expectedError.message );
            expect( BookService.findById ).toHaveBeenCalledWith( bookId );
        } );
    } );

    describe( 'GET /books/', () => {
        it( 'should retrieve a paginated list of books', async () => {
            const mockResult = {
                page: 1,
                limit: 10,
                total: 20,
                data: [
                    { _id: '123', title: 'Book 1', author: 'Author 1' },
                    { _id: '456', title: 'Book 2', author: 'Author 2' }
                ]
            };
            BookService.getPage.mockResolvedValue( mockResult );

            const response = await request( app )
                .get( `${PATH}/books` );

            expect( response.status ).toBe( 200 );
            expect( response.body ).toEqual( mockResult );
            expect( BookService.getPage ).toHaveBeenCalled();
        } );

        it( 'should return 404 Book not found error if bookId does not exist', async () => {
            const bookId = 'someId134231';

            const expectedError = errors[errorsEnum.BOOK_NOT_FOUND]( bookId );
            BookService.findById.mockRejectedValue( new CustomError( errorsEnum.BOOK_NOT_FOUND, bookId ) );

            const response = await request( app )
                .get( `${PATH}/books/${bookId}` );

            expect( response.status ).toBe( expectedError.code );
            expect( response.body.error.id ).toBe( expectedError.id );
            expect( response.body.error.message ).toBe( expectedError.message );
            expect( BookService.findById ).toHaveBeenCalledWith( bookId );
        } );
    } );

    describe( 'DELETE /books/:bookId', () => {
        it( 'should delete the book with the given bookId', async () => {
            const bookData = {
                _id: '123456789',
                title: 'Mock Title',
                author: 'Mock Author'
            };
            BookService.deleteById.mockResolvedValue( bookData );

            const response = await request( app )
                .delete( `${PATH}/books/${bookData._id}` );

            expect( response.status ).toBe( 200 );
            expect( response.body.data ).toEqual( bookData );
            expect( BookService.deleteById ).toHaveBeenCalledWith( bookData._id );
        } );

        it( 'should return 404 Book not found error if bookId does not exist', async () => {
            const bookId = 'someId134231';

            const expectedError = errors[errorsEnum.BOOK_NOT_FOUND]( bookId );
            BookService.deleteById.mockRejectedValue( new CustomError( errorsEnum.BOOK_NOT_FOUND, bookId ) );

            const response = await request( app )
                .delete( `${PATH}/books/${bookId}` );

            expect( response.status ).toBe( expectedError.code );
            expect( response.body.error.id ).toBe( expectedError.id );
            expect( response.body.error.message ).toBe( expectedError.message );
            expect( BookService.deleteById ).toHaveBeenCalledWith( bookId );
        } );
    } );
} );
