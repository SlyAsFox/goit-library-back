const router = require( 'express' ).Router();
const asyncHandler = require( 'express-async-handler' );
const BookService = require( '../../../../services/BookService' );
const { validationSchema } = require( '../../../../models/Book' );
const validateRequest = require( '../../../../middlewares/validateRequest' );

/**
 * @openapi
 * /books:
 *   post:
 *     tags:
 *       - Books
 *     summary: Create new book.
 *     description: Method for create new book.
 *     requestBody:
 *       description: |
 *          ## Method for create new book.
 *          ### Parameters:
 *
 *          | Field | Is Required |  Type | Description |
 *          | ------- | ------- | ------- | ------------------------------------------------ |
 *          | `title` | Required | String | Book title. |
 *          | `author` | Required | String | Book author. |
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/BookDTO'
 *     responses:
 *       201:
 *         description: Returns created book.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   $ref: '#/components/schemas/Book'
 *       415:
 *         description: Returns 415 Validation error.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ERROR'
 */
router.post( '/', validateRequest( validationSchema ), asyncHandler( async ( req, res ) => {
    const { title, author } = req.body;

    const result = await BookService.create( { title, author } );

    return res.status( 201 ).send( {
        data: result
    } );
} ) );

/**
 * @openapi
 * /books/{bookId}:
 *   put:
 *     tags:
 *       - Books
 *     summary: Update book.
 *     description: Method for update data in exist book.
 *     parameters:
 *       - in: path
 *         name: bookId
 *         required: true
 *         schema:
 *           type: string
 *     requestBody:
 *       description: |
 *          ## Method for update data in exist book.
 *          ### Parameters:
 *
 *          | Field | Is Required |  Type | Description |
 *          | ------- | ------- | ------- | ------------------------------------------------ |
 *          | `title` | Required | String | Book title. |
 *          | `author` | Required | String | Book author. |
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/BookDTO'
 *     responses:
 *       200:
 *         description: Returns updated book.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   $ref: '#/components/schemas/Book'
 *       404:
 *         description: Returns 404 Book not found error.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ERROR'
 *       415:
 *         description: Returns 415 Validation error.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ERROR'
 */
router.put( '/:bookId', validateRequest( validationSchema ), asyncHandler( async ( req, res ) => {
    const { title, author } = req.body;
    const { bookId } = req.params;

    const result = await BookService.updateById( bookId, { title, author } );

    return res.status( 200 ).send( {
        data: result
    } );
} ) );

/**
 * @openapi
 * /books/{bookId}:
 *   get:
 *     tags:
 *       - Books
 *     summary: Get book.
 *     description: Method for get exist book data.
 *     parameters:
 *       - in: path
 *         name: bookId
 *         required: true
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Returns book.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   $ref: '#/components/schemas/Book'
 *       404:
 *         description: Returns 404 Book not found error.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ERROR'
 */
router.get( '/:bookId', asyncHandler( async ( req, res ) => {
    const { bookId } = req.params;

    const result = await BookService.findById( bookId );

    return res.status( 200 ).send( {
        data: result
    } );
} ) );

/**
 * @openapi
 * /books:
 *   get:
 *     tags:
 *       - Books
 *     summary: Get books with pagination.
 *     description: 'Method for get books list with pagination( page, limit ). Default values: page - 1, limit - 10.'
 *     parameters:
 *       - in: query
 *         name: page
 *         required: false
 *         schema:
 *           type: string
 *       - in: query
 *         name: limit
 *         required: false
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Returns books list.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/Book'
 *                 totalPages:
 *                   type: number
 *                   description: Total pages count for provided limit.
 *                   example: 2
 *                 currentPage:
 *                   type: number
 *                   description: Current page number.
 *                   example: 1
 *       404:
 *         description: Returns 404 Book not found error.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ERROR'
 */
router.get( '/', asyncHandler( async ( req, res ) => {
    const { page, limit } = req.query;

    const result = await BookService.getPage( { page, limit } );

    return res.status( 200 ).send( result );
} ) );

/**
 * @openapi
 * /books/{bookId}:
 *   delete:
 *     tags:
 *       - Books
 *     summary: Delete book.
 *     description: Method for delete exist book by id.
 *     parameters:
 *       - in: path
 *         name: bookId
 *         required: true
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Returns book.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   $ref: '#/components/schemas/Book'
 *       404:
 *         description: Returns 404 Book not found error.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ERROR'
 */
router.delete( '/:bookId', asyncHandler( async ( req, res ) => {
    const { bookId } = req.params;

    const result = await BookService.deleteById( bookId );

    return res.status( 200 ).send( {
        data: result
    } );
} ) );


module.exports = router;

/**
 * @openapi
 * components:
 *   schemas:
 *     Book:
 *       type: object
 *       properties:
 *         _id:
 *           type: string
 *           description: Created book id.
 *           example: '1111a1c3c916e43d9155c906'
 *         title:
 *           type: string
 *           description: Created book title.
 *           example: 'The Great Gatsby'
 *         author:
 *           type: string
 *           description: Created book author.
 *           example: 'F. Scott Fitzgerald'
 *     BookDTO:
 *       type: object
 *       properties:
 *         title:
 *           type: string
 *           description: Created book title.
 *           example: 'The Great Gatsby'
 *         author:
 *           type: string
 *           description: Created book author.
 *           example: 'F. Scott Fitzgerald'
 *
 */
