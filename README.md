# goit-library-back

## Task
Напишіть веб-сервер, що реалізує CRUD-операції зі списком книг, що зберігаються у базі даних MongoDB. Книга є об'єктом з такими полями:
`title`;`author`;

На сервері мають бути доступні такі роути:
 - [x] отримання всіх книг з можливою пагінацією (параметри page та limit);
 - [x] отримання однієї книги з id;
 - [x] додавання книги із валідацією тіла запиту;
 - [x] оновлення книги з id (роут повинен повертати оновлену книгу);
 - [x] видалення книги з id; 

Для створення роутів використовуйте пакети express та cors. Для валідації тіла запиту – пакет Joi. Для роботи з базою даних MongoDB – бібліотеку mongoose. Додаткові бібліотеки використовуйте за бажанням. Виконане завдання завантажте на Github та задеплойте на render.com

***
# Overview

## Results

Project deployed using [render.com](https://render.com/)

Public link: [https://goit-library.onrender.com](https://goit-library.onrender.com)

Link to swagger spec: [https://goit-library.onrender.com/api-docs](https://goit-library.onrender.com/api-docs)

## Project Structure
The folder structure of this app is explained below:

| Name             | Description                                                                                              |
|------------------|----------------------------------------------------------------------------------------------------------|
| **errors**       | Contains errors configuration, class for throw custom error                                              |
| **middlewares**  | Express middlewares which process the incoming requests before handling them down to the routes          |
| **models**       | Models define schemas that will be used in storing, retrieving & validate data from Application database |
| **node_modules** | Contains all npm dependencies                                                                            |
| **routes**       | Contain all express routes, separated by module/area of application                                      |
| **services**     | Contain all services, separated by module of application, using for describe business logic              |
| .env             | Contain environment variables when project run not in production                                         |
| eslintrc.json    | Config settings for ESLint code style checking                                                           |
| app.js           | Node js app configuration                                                                                |
| dotenv.js        | Config environment variables when project run not in production for use process.env object               |
| mongoose.js      | Config connection to MongoDB                                                                             |
| package.json     | Contains npm dependencies as well as build scripts                                                       | 
| server.js        | Check environment variables & db connection, start server                                                | 

***
# Usage

## Environment vars
This project uses the following environment variables:

| Name      | Description                         |
|-----------| ------------------------------------|
| `NODE_ENV`  | This variable is used to specify the environment mode of the project, such as development, production, or testing. It helps in configuring different settings based on the environment.s            |
| `PORT`  | This variable represents the port number on which the project will run. It specifies the network port to listen for incoming requests           |
| `MONGO_DB_URL`  | This variable contains the URL or connection string for the MongoDB database. It is used to establish a connection with the MongoDB database server and perform database operations.            |

## Pre-requisites
- Install [Node.js](https://nodejs.org/en/) version 16.16.0

## Getting started
- Clone the repository
```
git clone  https://gitlab.com/SlyAsFox/goit-library-back.git
```
- Install dependencies
```
cd goit-library-back
npm install
```
- Run the project
```
npm start
```
Navigate to `http://localhost:{PORT}`

- API Document endpoints

  swagger Spec Endpoint : http://localhost:{PORT}/api-docs
