const http = require( 'http' );
const express = require( 'express' );
const cors = require( 'cors' );
const helmet = require( 'helmet' );
const swaggerUI = require( 'swagger-ui-express' );
const swaggerJsdoc = require( 'swagger-jsdoc' );
const errorHandler = require( './middlewares/errorHandler' );

const v1Routes = require( './routes/api/v1' );

// Main setup
const app = express();
app.use( cors() );
app.use( helmet() );
app.use( express.urlencoded( {
    extended: true
} ) );
app.use( express.json() );
app.use( ( req, res, next ) => {
    if ( process.env.NODE_ENV !== 'production' ) {
        console.log( `${req.method} | ${req.url}` );
    }
    next();
} );
app.get( '/health', ( req, res ) => {
    res.send( {
        data: 'OK'
    } );
} );

// Swagger setup
const swaggerOptions = {
    customCss: '.swagger-ui .topbar { display: none }',
    definition: {
        openapi: '3.0.0',
        info: {
            title: 'Books API Specification',
            description: 'This routes allow to manipulate with books.',
            contact: {
                name: 'API Support',
                email: 'slyasfox7@gmail.com'
            },
            version: '1.0'
        },
        servers: [
            {
                description: 'Public server (v1)',
                url: `https://goit-library.onrender.com/api/v1`
            },
            {
                description: 'Test server (v1)',
                url: `http://localhost:${process.env.PORT}/api/v1`
            }
        ],
        schemes: [ 'http' ]
    },
    apis: [ 'server.js', './routes/**/*.js', './errors/**/*.js' ]
};
const apiSpecification = swaggerJsdoc( swaggerOptions );

// Routes configuration
app.use( '/api-docs', swaggerUI.serve, swaggerUI.setup( apiSpecification ) );
app.use( '/api/v1', v1Routes );
app.use( errorHandler );

// Create HTTP server.
const server = http.createServer( app );

module.exports = { server, app };

/**
 * @openapi
 * tags:
 *   - name: Books
 *     description: Operations with books
 */
