const mongoose = require( 'mongoose' );

class Database {
    static async connect () {
        return mongoose.connect( process.env.MONGO_DB_URL, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        } );
    }
}

module.exports = Database;
