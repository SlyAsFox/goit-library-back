const errorsEnum = {
    SERVER_ERROR: 'SERVER_ERROR',
    VALIDATION_ERROR: 'VALIDATION_ERROR',
    BOOK_NOT_FOUND: 'BOOK_NOT_FOUND'
};

module.exports = Object.freeze( errorsEnum );
