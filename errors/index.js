module.exports = {
    CustomError: require( './CustomError' ),
    errorsEnum: require( './errorsEnum' ),
    errors: require( './errors' ),
};
