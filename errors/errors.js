const errorsEnum = require( './errorsEnum' );

const ERRORS = {
    VALIDATION_ERROR: ( msg ) => {
        return {
            id: errorsEnum.VALIDATION_ERROR,
            message: msg,
            code: 415
        };
    },
    BOOK_NOT_FOUND: ( id ) => {
        return {
            id: errorsEnum.BOOK_NOT_FOUND,
            message: `Book with provided id(${id}) not found`,
            code: 404
        };
    },
    SERVER_ERROR: ( error ) => {
        return {
            id: errorsEnum.SERVER_ERROR,
            message: `Something went wrong: ${error.message}`,
            code: 500
        };
    },
};

module.exports = ERRORS;
/**
 * @openapi
 * components:
 *   schemas:
 *     ERROR:
 *       type: object
 *       properties:
 *         error:
 *           type: object
 *           properties:
 *             id:
 *               type: string
 *               description: The Error id.
 *               example: 'VALIDATION_ERROR'
 *             message:
 *               type: string
 *               description: The Error message.
 *               example: '\"title\" is required'
 */
