if ( process.env.NODE_ENV !== 'production' ) {
    require( './dotenv' );
}
const mongoose = require( './mongoose' );
const { server } = require( './app' );


mongoose.connect()
    .then( () => {
        console.log( 'Mongo connected' );
        server.listen( process.env.PORT );
        console.log( `Server started at PORT: ${process.env.PORT}` );
    } )
    .catch( err => {
        throw err;
    } );
