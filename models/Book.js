const mongoose = require( 'mongoose' );
const Joi = require( 'joi' );

const bookSchema = new mongoose.Schema( {
    title: {
        type: String,
        required: true,
        minlength: 1,
        maxlength: 255
    },
    author: {
        type: String,
        required: true,
        minlength: 1,
        maxlength: 255
    }
} );

const Book = mongoose.model( 'Book', bookSchema );

const validationSchema = Joi.object( {
    title: Joi.string().min( 1 ).max( 255 ).required(),
    author: Joi.string().min( 1 ).max( 255 ).required()
} );

module.exports = {
    Book,
    validationSchema
};
